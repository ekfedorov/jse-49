package ru.ekfedorov.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.service.IReceiverService;
import ru.ekfedorov.tm.listener.LogMessageListener;
import ru.ekfedorov.tm.service.ActiveMQConnectionService;

public class Application {

    @SneakyThrows
    public static void main(final String[] args) {
        @NotNull final IReceiverService receiverService = new ActiveMQConnectionService();
        receiverService.receive(new LogMessageListener());
    }

}