package ru.ekfedorov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;

@NoArgsConstructor
public class ExecutorService implements ru.ekfedorov.tm.api.service.IExecutorService {

    @NotNull
    private final java.util.concurrent.ExecutorService executor = Executors.newFixedThreadPool(3);

    @Override
    public void submit(@NotNull final Runnable runnable) {
        executor.submit(runnable);
    }

}
